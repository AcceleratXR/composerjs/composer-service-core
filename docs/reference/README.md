**[@composer-js/service-core](README.md)**

> [Globals](globals.md)

# Composer: service_core

[![pipeline status](https://gitlab.com/AcceleratXR/composerjs/composer-service-core/badges/master/pipeline.svg)](https://gitlab.com/AcceleratXR/composerjs/composer-service-core/-/commits/master)
[![coverage report](https://gitlab.com/AcceleratXR/composerjs/composer-service-core/badges/master/coverage.svg)](https://gitlab.com/AcceleratXR/composerjs/composer-service-core/-/commits/master)

Provides common functionality and utilities for Composer based REST API service projects.

## NPM
```
npm i @composer-js/service_core
```

## Yarn
```
yarn add @composer-js/service_core
```
